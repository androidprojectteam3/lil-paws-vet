package com.example.lilpawsvet.RecycleAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lilpawsvet.Items.ListItem;
import com.example.lilpawsvet.R;

import java.util.ArrayList;

public class BreedRecyclerAdapter extends RecyclerView.Adapter<BreedRecyclerAdapter.BreedItemsHolder> {

    private ArrayList<ListItem> listItems;

    public BreedRecyclerAdapter(ArrayList<ListItem> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public BreedRecyclerAdapter.BreedItemsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.breed_item, parent, false);
        BreedRecyclerAdapter.BreedItemsHolder breedItemsHolder = new BreedRecyclerAdapter.BreedItemsHolder(view);
        return breedItemsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BreedRecyclerAdapter.BreedItemsHolder holder, int position) {
        holder.image.setImageResource(listItems.get(position).getImage());
        holder.title.setText(listItems.get(position).getTitle());
        holder.description.setText(listItems.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public static class BreedItemsHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title;
        TextView description;

        public BreedItemsHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
        }
    }
}