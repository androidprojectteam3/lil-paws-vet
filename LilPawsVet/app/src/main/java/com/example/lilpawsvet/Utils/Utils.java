package com.example.lilpawsvet.Utils;

import com.example.lilpawsvet.Activites.LoginActivity;
import com.example.lilpawsvet.Database.User;

import java.util.List;

public class Utils {

    public static boolean findUserForRegister(String username, String email) {
        List<User> users = LoginActivity.appDatabase.dao().getUsers();
        for (User user : users) {
            if (user.getUserName().trim().equals(username) || user.getUserEmail().trim().equals(email)) {
                return true;
            }
        }
        return false;
    }

    public static boolean findUserForLogin(String emailOrUsername, String password) {
        List<User> users = LoginActivity.appDatabase.dao().getUsers();
        for (User user : users) {
            if ((user.getUserName().trim().equals(emailOrUsername) && user.getPassword().trim().equals(password)) ||
                    (user.getUserEmail().trim().equals(emailOrUsername) && user.getPassword().trim().equals(password))) {
                return true;
            }
        }
        return false;
    }

    public static String getUserNameFromLogin(String emailOrUsername) {
        List<User> users = LoginActivity.appDatabase.dao().getUsers();
        for (User user : users) {
            if (user.getUserName().trim().equals(emailOrUsername) || user.getUserEmail().trim().equals(emailOrUsername)) {
                return user.getUserName();
            }
        }
        return "";
    }
}
