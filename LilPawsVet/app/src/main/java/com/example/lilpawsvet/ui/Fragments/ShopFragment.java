package com.example.lilpawsvet.ui.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.lilpawsvet.Items.ListItem;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.RecycleAdapters.ShopRecyclerAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class ShopFragment extends Fragment implements TabLayout.OnTabSelectedListener {
    private TabLayout tabs;
    private ArrayList<ListItem> shopList = new ArrayList<>();
    private View view;

    public static ShopFragment newInstance() {
        return new ShopFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shop_fragment, container, false);

        //Shop Tabs
        tabs = view.findViewById(R.id.shop_tab_layout);
        tabs.addOnTabSelectedListener(this);
        if (tabs.getSelectedTabPosition() != -1)
            onTabSelected(tabs.getTabAt(tabs.getSelectedTabPosition()));

        ImageButton filter = view.findViewById(R.id.button_filter_shop);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.filter_popup, popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return menuItemClick(item);
                    }
                });
            }
        });

        //Search Button
        ImageButton search = view.findViewById(R.id.button_search_shop);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText search = view.findViewById(R.id.text_search_shop);
                String search_text = search.getText().toString();
                if (search_text.isEmpty()) {
                    onTabSelected(tabs.getTabAt(tabs.getSelectedTabPosition()));
                } else {
                    for (int index = 0; index < shopList.size(); index++) {
                        if (!shopList.get(index).getTitle().toLowerCase().contains(search_text.toLowerCase())) {
                            shopList.remove(index);
                            index--;
                        }
                    }
                    refreshRecycleView();
                    search.setText("");
                }
            }
        });
        return view;
    }

    public boolean menuItemClick(MenuItem item) {
        if (tabs.getSelectedTabPosition() == 0) {
            switch (item.getItemId()) {
                case R.id.pick_dogs: {
                    changePetDog();
                    refreshRecycleView();
                    return true;
                }
                case R.id.pick_cats: {
                    changePetCat();
                    refreshRecycleView();
                    return true;
                }
                case R.id.pick_birds: {
                    changePetBird();
                    refreshRecycleView();
                    return true;
                }
                case R.id.pick_fish: {
                    changePetFish();
                    refreshRecycleView();
                    return true;
                }
                case R.id.pick_reptilians: {
                    changePetReptile();
                    refreshRecycleView();
                    return true;
                }
                case R.id.pick_rodents: {
                    changePetRodent();
                    refreshRecycleView();
                    return true;
                }
                case R.id.pick_all: {
                    changeToFood();
                    refreshRecycleView();
                    return true;
                }
                default:
                    return false;
            }
        } else {
            changeToMeds();
            refreshRecycleView();
            return true;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        EditText search = view.findViewById(R.id.text_search_shop);
        search.setText(null);
        if (tabs.getSelectedTabPosition() == 0)
            changeToFood();
        else
            changeToMeds();

        refreshRecycleView();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void refreshRecycleView() {
        RecyclerView mRecyclerView = view.findViewById(R.id.list_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView.Adapter mAdapter = new ShopRecyclerAdapter(shopList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void changeToFood() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.adult_salmon_potato_food, getResources().getString(R.string.adult_salmon_potato_food_title), getResources().getString(R.string.adult_salmon_potato_food_description)));
        shopList.add(new ListItem(R.drawable.beagle_food, getResources().getString(R.string.beagle_food_title), getResources().getString(R.string.beagle_food_description)));
        shopList.add(new ListItem(R.drawable.betta_enjoy_food, getResources().getString(R.string.betta_enjoy_food_title), getResources().getString(R.string.betta_enjoy_food_description)));
        shopList.add(new ListItem(R.drawable.betta_tropical_food, getResources().getString(R.string.betta_tropical_food_title), getResources().getString(R.string.betta_tropical_food_description)));
        shopList.add(new ListItem(R.drawable.boxer_food, getResources().getString(R.string.boxer_food_title), getResources().getString(R.string.boxer_food_description)));
        shopList.add(new ListItem(R.drawable.bulldog_food, getResources().getString(R.string.bulldog_food_title), getResources().getString(R.string.bulldog_food_description)));
        shopList.add(new ListItem(R.drawable.chihuahua_food, getResources().getString(R.string.chihuahua_food_title), getResources().getString(R.string.chihuahua_food_description)));
        shopList.add(new ListItem(R.drawable.deli_nature_food, getResources().getString(R.string.deli_nature_food_title), getResources().getString(R.string.deli_nature_food_description)));
        shopList.add(new ListItem(R.drawable.german_shepherd_food, getResources().getString(R.string.german_shepherd_food_title), getResources().getString(R.string.german_shepherd_food_description)));
        shopList.add(new ListItem(R.drawable.golden_retriever_food, getResources().getString(R.string.golden_retriever_food_title), getResources().getString(R.string.golden_retriever_food_description)));
        shopList.add(new ListItem(R.drawable.gourmet_curcan_food, getResources().getString(R.string.gourmet_curcan_food_title), getResources().getString(R.string.gourmet_curcan_food_description)));

        shopList.add(new ListItem(R.drawable.guineea_pig_food, getResources().getString(R.string.guineea_pig_food_title), getResources().getString(R.string.guineea_pig_food_description)));
        shopList.add(new ListItem(R.drawable.lizard_food, getResources().getString(R.string.lizard_food_title), getResources().getString(R.string.lizard_food_description)));
        shopList.add(new ListItem(R.drawable.maxi_puppy_food, getResources().getString(R.string.maxi_puppy_food_title), getResources().getString(R.string.maxi_puppy_food_description)));
        shopList.add(new ListItem(R.drawable.mini_puppy_food, getResources().getString(R.string.mini_puppy_food_title), getResources().getString(R.string.mini_puppy_food_description)));
        shopList.add(new ListItem(R.drawable.pedigree_beef_food, getResources().getString(R.string.pedigree_beef_food_title), getResources().getString(R.string.pedigree_beef_food_description)));
        shopList.add(new ListItem(R.drawable.poodle_food, getResources().getString(R.string.poodle_food_title), getResources().getString(R.string.poodle_food_description)));
        shopList.add(new ListItem(R.drawable.promo_adult_mini_food, getResources().getString(R.string.promo_adult_mini_food_title), getResources().getString(R.string.promo_adult_mini_food_description)));
        shopList.add(new ListItem(R.drawable.pug_food, getResources().getString(R.string.pug_food_title), getResources().getString(R.string.pug_food_description)));
        shopList.add(new ListItem(R.drawable.purina_adult_food, getResources().getString(R.string.purina_adult_food_title), getResources().getString(R.string.purina_adult_food_description)));
        shopList.add(new ListItem(R.drawable.purina_junior_food, getResources().getString(R.string.purina_junior_food_title), getResources().getString(R.string.purina_junior_food_description)));
        shopList.add(new ListItem(R.drawable.purina_sterilecat_food, getResources().getString(R.string.purina_sterilecat_food_title), getResources().getString(R.string.purina_sterilecat_food_description)));

        shopList.add(new ListItem(R.drawable.rottweiler_food, getResources().getString(R.string.rottweiler_food_title), getResources().getString(R.string.rottweiler_food_description)));
        shopList.add(new ListItem(R.drawable.turtle_30ml_food, getResources().getString(R.string.turtle_30ml_food_title), getResources().getString(R.string.turtle_30ml_food_description)));
        shopList.add(new ListItem(R.drawable.turtle_menu_food, getResources().getString(R.string.turtle_menu_food_title), getResources().getString(R.string.turtle_menu_food_description)));
        shopList.add(new ListItem(R.drawable.turtle_sticks_food, getResources().getString(R.string.turtle_sticks_food_title), getResources().getString(R.string.turtle_sticks_food_description)));
        shopList.add(new ListItem(R.drawable.versele_food, getResources().getString(R.string.versele_food_title), getResources().getString(R.string.versele_food_description)));
        shopList.add(new ListItem(R.drawable.vitakraft_egg_food, getResources().getString(R.string.vitakraft_egg_food_title), getResources().getString(R.string.vitakraft_egg_food_description)));
        shopList.add(new ListItem(R.drawable.vitakraft_hamster_food, getResources().getString(R.string.vitakraft_hamster_food_title), getResources().getString(R.string.vitakraft_hamster_food_description)));
        shopList.add(new ListItem(R.drawable.vitakraft_rabbit_food, getResources().getString(R.string.vitakraft_rabbit_food_title), getResources().getString(R.string.vitakraft_rabbit_food_description)));
        shopList.add(new ListItem(R.drawable.whiskas_adult_pui, getResources().getString(R.string.whiskas_adult_pui_title), getResources().getString(R.string.whiskas_adult_pui_description)));
        shopList.add(new ListItem(R.drawable.worm_food, getResources().getString(R.string.worm_food_title), getResources().getString(R.string.worm_food_description)));
        shopList.add(new ListItem(R.drawable.yorkshire_terrier_food, getResources().getString(R.string.yorkshire_terrier_food_title), getResources().getString(R.string.yorkshire_terrier_food_description)));

    }

    public void changeToMeds() {
        shopList.clear();
        shopList.add(new ListItem(R.drawable.ace, getResources().getString(R.string.ace_title), getResources().getString(R.string.ace_description)));
        shopList.add(new ListItem(R.drawable.artrivet_forte, getResources().getString(R.string.artrivet_forte_title), getResources().getString(R.string.artrivet_forte_description)));
        shopList.add(new ListItem(R.drawable.bandaje_cohezive, getResources().getString(R.string.bandaje_cohezive_title), getResources().getString(R.string.bandaje_cohezive_description)));
        shopList.add(new ListItem(R.drawable.biseptyl, getResources().getString(R.string.biseptyl_title), getResources().getString(R.string.biseptyl_description)));
        shopList.add(new ListItem(R.drawable.fiprodog, getResources().getString(R.string.fiprodog_title), getResources().getString(R.string.fiprodog_description)));
        shopList.add(new ListItem(R.drawable.florfenidem, getResources().getString(R.string.florfenidem_100ml_title), getResources().getString(R.string.florfenidem_100ml_description)));

        shopList.add(new ListItem(R.drawable.florfenidem, getResources().getString(R.string.florfenidem_1l_title), getResources().getString(R.string.florfenidem_1l_description)));
        shopList.add(new ListItem(R.drawable.levaverm, getResources().getString(R.string.levaverm_title), getResources().getString(R.string.levaverm_description)));
        shopList.add(new ListItem(R.drawable.neocaf_spray, getResources().getString(R.string.neocaf_spray_title), getResources().getString(R.string.neocaf_spray_description)));
        shopList.add(new ListItem(R.drawable.syvaquinol, getResources().getString(R.string.syvaquinol_1l_title), getResources().getString(R.string.syvaquinol_1l_description)));
        shopList.add(new ListItem(R.drawable.syvaquinol, getResources().getString(R.string.syvaquinol_5l_title), getResources().getString(R.string.syvaquinol_5l_description)));
        shopList.add(new ListItem(R.drawable.t61, getResources().getString(R.string.t61_title), getResources().getString(R.string.t61_description)));
        shopList.add(new ListItem(R.drawable.veterelin, getResources().getString(R.string.veterelin_title), getResources().getString(R.string.veterelin_description)));

    }

    public void changePetDog() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.adult_salmon_potato_food, getResources().getString(R.string.adult_salmon_potato_food_title), getResources().getString(R.string.adult_salmon_potato_food_description)));
        shopList.add(new ListItem(R.drawable.beagle_food, getResources().getString(R.string.beagle_food_title), getResources().getString(R.string.beagle_food_description)));
        shopList.add(new ListItem(R.drawable.boxer_food, getResources().getString(R.string.boxer_food_title), getResources().getString(R.string.boxer_food_description)));
        shopList.add(new ListItem(R.drawable.bulldog_food, getResources().getString(R.string.bulldog_food_title), getResources().getString(R.string.bulldog_food_description)));
        shopList.add(new ListItem(R.drawable.chihuahua_food, getResources().getString(R.string.chihuahua_food_title), getResources().getString(R.string.chihuahua_food_description)));
        shopList.add(new ListItem(R.drawable.german_shepherd_food, getResources().getString(R.string.german_shepherd_food_title), getResources().getString(R.string.german_shepherd_food_description)));
        shopList.add(new ListItem(R.drawable.golden_retriever_food, getResources().getString(R.string.golden_retriever_food_title), getResources().getString(R.string.golden_retriever_food_description)));

        shopList.add(new ListItem(R.drawable.maxi_puppy_food, getResources().getString(R.string.maxi_puppy_food_title), getResources().getString(R.string.maxi_puppy_food_description)));
        shopList.add(new ListItem(R.drawable.mini_puppy_food, getResources().getString(R.string.mini_puppy_food_title), getResources().getString(R.string.mini_puppy_food_description)));
        shopList.add(new ListItem(R.drawable.pedigree_beef_food, getResources().getString(R.string.pedigree_beef_food_title), getResources().getString(R.string.pedigree_beef_food_description)));
        shopList.add(new ListItem(R.drawable.poodle_food, getResources().getString(R.string.poodle_food_title), getResources().getString(R.string.poodle_food_description)));
        shopList.add(new ListItem(R.drawable.promo_adult_mini_food, getResources().getString(R.string.promo_adult_mini_food_title), getResources().getString(R.string.promo_adult_mini_food_description)));
        shopList.add(new ListItem(R.drawable.pug_food, getResources().getString(R.string.pug_food_title), getResources().getString(R.string.pug_food_description)));
        shopList.add(new ListItem(R.drawable.rottweiler_food, getResources().getString(R.string.rottweiler_food_title), getResources().getString(R.string.rottweiler_food_description)));
        shopList.add(new ListItem(R.drawable.yorkshire_terrier_food, getResources().getString(R.string.yorkshire_terrier_food_title), getResources().getString(R.string.yorkshire_terrier_food_description)));
    }

    public void changePetCat() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.gourmet_curcan_food, getResources().getString(R.string.gourmet_curcan_food_title), getResources().getString(R.string.gourmet_curcan_food_description)));
        shopList.add(new ListItem(R.drawable.purina_adult_food, getResources().getString(R.string.purina_adult_food_title), getResources().getString(R.string.purina_adult_food_description)));
        shopList.add(new ListItem(R.drawable.purina_junior_food, getResources().getString(R.string.purina_junior_food_title), getResources().getString(R.string.purina_junior_food_description)));
        shopList.add(new ListItem(R.drawable.purina_sterilecat_food, getResources().getString(R.string.purina_sterilecat_food_title), getResources().getString(R.string.purina_sterilecat_food_description)));
        shopList.add(new ListItem(R.drawable.whiskas_adult_pui, getResources().getString(R.string.whiskas_adult_pui_title), getResources().getString(R.string.whiskas_adult_pui_description)));
    }

    public void changePetBird() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.deli_nature_food, getResources().getString(R.string.deli_nature_food_title), getResources().getString(R.string.deli_nature_food_description)));
        shopList.add(new ListItem(R.drawable.versele_food, getResources().getString(R.string.versele_food_title), getResources().getString(R.string.versele_food_description)));
        shopList.add(new ListItem(R.drawable.vitakraft_egg_food, getResources().getString(R.string.vitakraft_egg_food_title), getResources().getString(R.string.vitakraft_egg_food_description)));
        shopList.add(new ListItem(R.drawable.worm_food, getResources().getString(R.string.worm_food_title), getResources().getString(R.string.worm_food_description)));
    }

    public void changePetRodent() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.guineea_pig_food, getResources().getString(R.string.guineea_pig_food_title), getResources().getString(R.string.guineea_pig_food_description)));
        shopList.add(new ListItem(R.drawable.vitakraft_hamster_food, getResources().getString(R.string.vitakraft_hamster_food_title), getResources().getString(R.string.vitakraft_hamster_food_description)));
        shopList.add(new ListItem(R.drawable.vitakraft_rabbit_food, getResources().getString(R.string.vitakraft_rabbit_food_title), getResources().getString(R.string.vitakraft_rabbit_food_description)));
        shopList.add(new ListItem(R.drawable.worm_food, getResources().getString(R.string.worm_food_title), getResources().getString(R.string.worm_food_description)));
    }

    public void changePetReptile() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.lizard_food, getResources().getString(R.string.lizard_food_title), getResources().getString(R.string.lizard_food_description)));
        shopList.add(new ListItem(R.drawable.turtle_30ml_food, getResources().getString(R.string.turtle_30ml_food_title), getResources().getString(R.string.turtle_30ml_food_description)));
        shopList.add(new ListItem(R.drawable.turtle_menu_food, getResources().getString(R.string.turtle_menu_food_title), getResources().getString(R.string.turtle_menu_food_description)));
        shopList.add(new ListItem(R.drawable.turtle_sticks_food, getResources().getString(R.string.turtle_sticks_food_title), getResources().getString(R.string.turtle_sticks_food_description)));
        shopList.add(new ListItem(R.drawable.worm_food, getResources().getString(R.string.worm_food_title), getResources().getString(R.string.worm_food_description)));
    }

    public void changePetFish() {
        shopList.clear();

        shopList.add(new ListItem(R.drawable.betta_enjoy_food, getResources().getString(R.string.betta_enjoy_food_title), getResources().getString(R.string.betta_enjoy_food_description)));
        shopList.add(new ListItem(R.drawable.betta_tropical_food, getResources().getString(R.string.betta_tropical_food_title), getResources().getString(R.string.betta_tropical_food_description)));
    }

}