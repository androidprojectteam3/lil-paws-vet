package com.example.lilpawsvet.ui.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lilpawsvet.Activites.LoginActivity;
import com.example.lilpawsvet.Database.User;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.Utils.Utils;

public class RegisterFragment extends Fragment {

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment, container, false);

        final EditText username = view.findViewById(R.id.username);
        final EditText email = view.findViewById(R.id.email);
        final EditText parola = view.findViewById(R.id.parola);
        final EditText confirmareParola = view.findViewById(R.id.confirmare_parola);

        //Register Button
        Button register = view.findViewById(R.id.spre_login);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User newUser = new User();
                String newUsername = username.getText().toString().trim();
                String newEmail = email.getText().toString().trim();
                String newPassword = parola.getText().toString().trim();
                String newConfirmPassword = confirmareParola.getText().toString().trim();
                newUser.setUserName(newUsername);
                newUser.setPassword(newPassword);
                newUser.setUserEmail(newEmail);

                if (newUsername.equals("") || newEmail.equals("") || newPassword.equals("") || newConfirmPassword.equals("")) {
                    Toast.makeText(getActivity(), R.string.not_all_fields_set, Toast.LENGTH_SHORT).show();
                } else if (!newPassword.equals(newConfirmPassword)) {
                    Toast.makeText(getActivity(), R.string.wrong_password, Toast.LENGTH_SHORT).show();
                } else if (Utils.findUserForRegister(newUsername, newEmail)) {
                    Toast.makeText(getActivity(), R.string.username_or_email_already_taken, Toast.LENGTH_SHORT).show();
                } else {
                    LoginActivity.appDatabase.dao().addUser(newUser);
                    username.setText("");
                    email.setText("");
                    parola.setText("");
                    confirmareParola.setText("");
                    Toast.makeText(getActivity(), R.string.register_successful, Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}