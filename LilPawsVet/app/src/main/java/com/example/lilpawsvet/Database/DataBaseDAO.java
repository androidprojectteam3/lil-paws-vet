package com.example.lilpawsvet.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface DataBaseDAO {

    @Insert
    void addUser(User user);

    @Query("select * from users")
    List<User> getUsers();

    @Delete
    void deleteUser(User user);

    @Insert
    void addAppointment(Appointment appointment);

    @Query("select * from appointments")
    List<Appointment> getAppointments();

    @Delete
    void deleteAppointment(Appointment appointment);

    @Transaction
    @Query("SELECT * FROM users")
    List<UserWithAppointments> getUserWithAppointments();
}
