package com.example.lilpawsvet.ui.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lilpawsvet.Activites.MenuActivity;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.Utils.Utils;

public class LoginFragment extends Fragment {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);

        final EditText etEmail = view.findViewById(R.id.email_or_user_login);
        final EditText etPassword = view.findViewById(R.id.password_login);

        //Login Button
        Button buttonToMenu = view.findViewById(R.id.sign_in_button);
        buttonToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailOrUserName = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                if (Utils.findUserForLogin(emailOrUserName, password)) {
                    requireActivity().startActivity(new Intent(requireActivity(), MenuActivity.class));
                    etEmail.setText("");
                    etPassword.setText("");
                } else {
                    Toast.makeText(getActivity(), R.string.incorrect_user_or_password, Toast.LENGTH_SHORT).show();
                }

                String userName = Utils.getUserNameFromLogin(emailOrUserName);
                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("username", userName);
                editor.commit();
            }
        });

        //Register Button
        Button buttonToRegister = view.findViewById(R.id.register_button);
        buttonToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_login_activity, new RegisterFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });
        return view;
    }
}