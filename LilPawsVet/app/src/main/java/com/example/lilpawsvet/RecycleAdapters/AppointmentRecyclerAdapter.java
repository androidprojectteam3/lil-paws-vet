package com.example.lilpawsvet.RecycleAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lilpawsvet.Activites.LoginActivity;
import com.example.lilpawsvet.Database.Appointment;
import com.example.lilpawsvet.R;

import java.util.List;

public class AppointmentRecyclerAdapter extends RecyclerView.Adapter<AppointmentRecyclerAdapter.AppointmentItemsHolder> {

    private List<Appointment> listItems;

    public AppointmentRecyclerAdapter(List<Appointment> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public AppointmentRecyclerAdapter.AppointmentItemsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_item, parent, false);
        AppointmentRecyclerAdapter.AppointmentItemsHolder appointmentItemsHolder = new AppointmentRecyclerAdapter.AppointmentItemsHolder(view);
        return appointmentItemsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AppointmentRecyclerAdapter.AppointmentItemsHolder holder, final int position) {
        holder.date.setText(listItems.get(position).getAppointmentDate());
        holder.description.setText(listItems.get(position).getDetails());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Appointment deletedAppointment = new Appointment();
                deletedAppointment.setId(listItems.get(position).getId());
                listItems.remove(position);
                LoginActivity.appDatabase.dao().deleteAppointment(deletedAppointment);
                notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public static class AppointmentItemsHolder extends RecyclerView.ViewHolder {

        TextView date;
        TextView description;
        ImageButton delete;

        public AppointmentItemsHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.text_appointment_date);
            description = itemView.findViewById(R.id.appointment_description);
            delete = itemView.findViewById(R.id.delete_button);
        }
    }
}