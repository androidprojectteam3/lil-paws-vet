package com.example.lilpawsvet.ui.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.lilpawsvet.R;

public class InfoFragment extends Fragment {


    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_fragment, container, false);

        //Button to open the cat breeds
        ImageButton cats = view.findViewById(R.id.info_photo_1);
        cats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("category", "cats");
                editor.commit();

                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new BreedFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });

        //Button to open the dog breeds
        ImageButton dogs = view.findViewById(R.id.info_photo_2);
        dogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("category", "dogs");
                editor.commit();

                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new BreedFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });

        //Button to open the bird breeds
        ImageButton birds = view.findViewById(R.id.info_photo_5);
        birds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("category", "birds");
                editor.commit();

                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new BreedFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });

        //Button to open the rodents breeds
        ImageButton rodents = view.findViewById(R.id.info_photo_3);
        rodents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("category", "rodents");
                editor.commit();

                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new BreedFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });

        //Button to open the fish breeds
        ImageButton fish = view.findViewById(R.id.info_photo_4);
        fish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("category", "fish");
                editor.commit();

                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new BreedFragment()).addToBackStack("tag");
                fragmentTransaction.commit();

            }
        });

        //Button to open the reptile breeds
        ImageButton reptiles = view.findViewById(R.id.info_photo_6);
        reptiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("category", "reptiles");
                editor.apply();
                editor.commit();

                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new BreedFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });
        return view;
    }
}