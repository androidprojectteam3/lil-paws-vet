package com.example.lilpawsvet.Database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "appointments", foreignKeys = @ForeignKey(entity = User.class, parentColumns = "user_name", childColumns = "appointment_of_user", onDelete = CASCADE))
public class Appointment {


    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "appointment_of_user")
    private String userName;

    @NonNull
    @ColumnInfo(name = "appointment_date")
    private String appointmentDate;

    @NonNull
    @ColumnInfo(name = "appointment_details")
    private String details;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getUserName() {
        return userName;
    }

    public void setUserName(@NonNull String userName) {
        this.userName = userName;
    }

    @NonNull
    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(@NonNull String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    @NonNull
    public String getDetails() {
        return details;
    }

    public void setDetails(@NonNull String details) {
        this.details = details;
    }
}
