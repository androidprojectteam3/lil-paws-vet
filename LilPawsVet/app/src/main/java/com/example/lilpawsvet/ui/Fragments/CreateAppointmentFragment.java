package com.example.lilpawsvet.ui.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.lilpawsvet.Activites.LoginActivity;
import com.example.lilpawsvet.Database.Appointment;
import com.example.lilpawsvet.R;

import java.util.Calendar;

public class CreateAppointmentFragment extends Fragment {

    public static CreateAppointmentFragment newInstance() {
        return new CreateAppointmentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_create_appointment, container, false);

        final Calendar calendar = Calendar.getInstance();
        final TextView txtTime = view.findViewById(R.id.text_setup_hour_appointment);
        final TextView txtDate = view.findViewById(R.id.text_setup_date_appointment);

        //Set Hour Button
        Button buttonSetHour = view.findViewById(R.id.button_setup_hour_appointment);
        buttonSetHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDate.getText().toString().equals("No date set yet")) {
                    Toast.makeText(getActivity(), R.string.date_warning, Toast.LENGTH_SHORT).show();
                } else {
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minutes = calendar.get(Calendar.MINUTE);
                    TimePickerDialog pickerTime = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                                    calendar.set(Calendar.HOUR_OF_DAY, sHour);
                                    calendar.set(Calendar.MINUTE, sMinute);
                                    calendar.set(Calendar.SECOND, 0);
                                    if (sMinute < 10) {
                                        txtTime.setText(Integer.toString(sHour) + ":0" + Integer.toString(sMinute));
                                    } else {
                                        txtTime.setText(sHour + ":" + sMinute);
                                    }
                                }
                            }, hour, minutes, true);
                    pickerTime.show();
                }
            }
        });

        //Set Date Button
        Button buttonSetDate = view.findViewById(R.id.button_setup_date_appointment);
        buttonSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                // date picker dialog
                DatePickerDialog pickerDate = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker dp, int year, int monthOfYear, int dayOfMonth) {
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.YEAR, year);
                                txtDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                txtTime.setText("No time set yet");
                            }
                        }, year, month, day);
                pickerDate.show();
            }
        });

        //Add appointment Button
        Button buttonAddAppointment = view.findViewById(R.id.add_current_appointment);
        buttonAddAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText details = view.findViewById(R.id.edit_description_appointment);
                if (txtTime.getText().toString().equals("No time set yet") || txtDate.getText().toString().equals("No date set yet") ||
                        details.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.not_all_fields_set, Toast.LENGTH_SHORT).show();
                } else if (calendar.before(Calendar.getInstance())) {
                    Toast.makeText(getActivity(), R.string.no_date_before_today, Toast.LENGTH_SHORT).show();
                } else {
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, 30);
                    if (calendar.after(c)) {
                        Toast.makeText(getActivity(), R.string.no_over_a_month, Toast.LENGTH_SHORT).show();
                    } else if (calendar.get(Calendar.HOUR_OF_DAY) < 8 || calendar.get(Calendar.HOUR_OF_DAY) > 14) {
                        Toast.makeText(getActivity(), R.string.program_warning, Toast.LENGTH_SHORT).show();
                    } else {
                        Appointment appointment = new Appointment();
                        SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                        String username = preferences.getString("username", null);
                        appointment.setUserName(username);

                        appointment.setAppointmentDate(txtTime.getText().toString() + " " + txtDate.getText().toString());

                        appointment.setDetails(details.getText().toString());
                        LoginActivity.appDatabase.dao().addAppointment(appointment);
                        Toast.makeText(getActivity(), R.string.successful_appointment, Toast.LENGTH_SHORT).show();
                        txtTime.setText("No time set yet");
                        txtDate.setText("No date set yet");
                        details.setText("");
                    }
                }
            }
        });
        return view;
    }
}