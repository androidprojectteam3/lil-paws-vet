package com.example.lilpawsvet.Database;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {User.class, Appointment.class},version = 1)
public abstract class Database extends RoomDatabase {

    public abstract DataBaseDAO dao();
}