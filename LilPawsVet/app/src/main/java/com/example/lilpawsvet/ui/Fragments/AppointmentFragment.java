package com.example.lilpawsvet.ui.Fragments;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.lilpawsvet.Activites.LoginActivity;
import com.example.lilpawsvet.AlarmManager.AlertReceiver;
import com.example.lilpawsvet.Database.Appointment;
import com.example.lilpawsvet.Database.UserWithAppointments;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.RecycleAdapters.AppointmentRecyclerAdapter;

import java.util.Calendar;
import java.util.List;

public class AppointmentFragment extends Fragment {
    private List<Appointment> appointmentItems;

    public static AppointmentFragment newInstance() {
        return new AppointmentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.appointment_fragment, container, false);
        setAppointmentItems();
        final Calendar calendar = Calendar.getInstance();
        final TextView textTimeReminder = view.findViewById(R.id.text_time_for_reminder);
        final TextView textDateReminder = view.findViewById(R.id.text_date_for_reminder);

        //Set time Button
        Button setTimeForReminder = view.findViewById(R.id.button_add_time_for_reminder);
        setTimeForReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textDateReminder.getText().toString().equals("Date not set yet")) {
                    Toast.makeText(getActivity(), R.string.date_warning, Toast.LENGTH_SHORT).show();
                } else {
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minutes = calendar.get(Calendar.MINUTE);
                    TimePickerDialog pickerTime = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                            calendar.set(Calendar.HOUR_OF_DAY, sHour);
                            calendar.set(Calendar.MINUTE, sMinute);
                            calendar.set(Calendar.SECOND, 0);
                            if (sMinute < 10) {
                                textTimeReminder.setText(sHour + ":0" + sMinute);
                            } else {
                                textTimeReminder.setText(sHour + ":" + sMinute);
                            }
                        }
                    }, hour, minutes, true);
                    pickerTime.show();
                }
            }
        });

        //Set Date Button
        Button setDateForReminder = view.findViewById(R.id.button_add_date_for_reminder);
        setDateForReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog pickerDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker dp, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.YEAR, year);
                        textDateReminder.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        textTimeReminder.setText("Time not set yet");
                    }
                }, year, month, day);
                pickerDate.show();
            }
        });

        //Set reminder Button
        Button setReminder = view.findViewById(R.id.button_add_reminder);
        setReminder.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if (textTimeReminder.getText().toString().equals("Time not set yet") || textDateReminder.getText().toString().equals("Date not set yet")) {
                    Toast.makeText(getActivity(), R.string.not_all_fields_set, Toast.LENGTH_SHORT).show();
                } else if (calendar.before(Calendar.getInstance())) {
                    Toast.makeText(getActivity(), R.string.no_date_before_today, Toast.LENGTH_SHORT).show();
                } else {
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, 30);
                    if (calendar.after(c)) {
                        Toast.makeText(getActivity(), R.string.no_over_a_month, Toast.LENGTH_SHORT).show();
                    } else {
                        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(getContext(), AlertReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 1, intent, 0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                        Toast.makeText(getActivity(), "Reminder set successfully at " + calendar.getTime().toString(), Toast.LENGTH_LONG).show();
                        textDateReminder.setText("Date not set yet");
                        textTimeReminder.setText("Time not set yet");
                    }
                }
            }
        });

        //Add appointment Button
        ImageButton addAppointment = view.findViewById(R.id.button_add_appointment);
        addAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_appointment_activity, new CreateAppointmentFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });

        //Search Button
        ImageButton search = view.findViewById(R.id.button_search_appointment);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText search = view.findViewById(R.id.text_search_appointment);
                String searchText = search.getText().toString();
                if (!searchText.isEmpty()) {
                    for (int index = 0; index < appointmentItems.size(); index++) {
                        if (!appointmentItems.get(index).getDetails().toLowerCase().contains(searchText.toLowerCase())) {
                            appointmentItems.remove(index);
                            index--;
                        }
                    }
                } else {
                    setAppointmentItems();
                }

                refreshRecycleView(view);
                search.setText("");
            }
        });

        refreshRecycleView(view);
        return view;
    }

    //Used to refresh the RecycleView after
    public void refreshRecycleView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.list_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        AppointmentRecyclerAdapter adapter = new AppointmentRecyclerAdapter(appointmentItems);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    //Used to set the items for the recycle view
    public void setAppointmentItems() {
        List<UserWithAppointments> allUsers = LoginActivity.appDatabase.dao().getUserWithAppointments();
        SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        String username = preferences.getString("username", null);
        for (UserWithAppointments user : allUsers) {
            if (user.getUser().getUserName().equals(username)) {
                appointmentItems = user.getAppointments();
                break;
            }
        }
    }

}