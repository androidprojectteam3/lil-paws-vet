package com.example.lilpawsvet.Database;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class UserWithAppointments {
    @Embedded
    private User user;
    @Relation(
            parentColumn = "user_name",
            entityColumn = "appointment_of_user"
    )
    private List<Appointment> appointments;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }
}
