package com.example.lilpawsvet.ui.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lilpawsvet.Items.ListItem;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.RecycleAdapters.BreedRecyclerAdapter;

import java.util.ArrayList;

public class BreedFragment extends Fragment {

    private ArrayList<ListItem> breedItems;

    public static BreedFragment newInstance() {
        return new BreedFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.breed_fragment, container, false);

        //Get the category of animals from shared preferences
        SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        String category = preferences.getString("category", null);
        breedItems = getCategory(category);

        //Back Button
        ImageButton back = view.findViewById(R.id.button_back_info);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_info_activity, new InfoFragment());
                fragmentTransaction.commit();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        //Search Button
        ImageButton search = view.findViewById(R.id.button_search_info);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText search = view.findViewById(R.id.text_search_info);
                String searchText = search.getText().toString();

                SharedPreferences preferences = getActivity().getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                String category = preferences.getString("category", null);

                if (searchText.isEmpty()) {
                    assert category != null;
                    breedItems = getCategory(category);
                } else
                    for (int index = 0; index < breedItems.size(); index++) {
                        if (!breedItems.get(index).getTitle().toLowerCase().contains(searchText.toLowerCase())) {
                            breedItems.remove(index);
                            index--;
                        }
                    }
                refreshRecycleView(view);
                search.setText("");
            }
        });

        refreshRecycleView(view);
        return view;
    }


    //Function to get the breed Category
    public ArrayList<ListItem> getCategory(String category) {
        switch (category) {
            case "dogs":
                return getDogs();

            case "cats":
                return getCats();

            case "birds":
                return getBirds();

            case "fish":
                return getFish();

            case "reptiles":
                return getReptiles();

            case "rodents":
                return getRodents();
        }

        return new ArrayList<>();
    }

    //Function to refresh the recycle view
    public void refreshRecycleView(View view) {
        RecyclerView mRecyclerView = view.findViewById(R.id.list_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView.Adapter mAdapter = new BreedRecyclerAdapter(breedItems);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }


    public ArrayList<ListItem> getDogs() {
        ArrayList<ListItem> breedItems = new ArrayList<>();
        breedItems.add(new ListItem(R.drawable.ciobanesc_german_photo, getResources().getString(R.string.german_shepherd_info_title), getResources().getString(R.string.german_shepherd_info_description)));
        breedItems.add(new ListItem(R.drawable.golden_retriver_photo, getResources().getString(R.string.golden_retriver_info_title), getResources().getString(R.string.golden_retriver_info_description)));
        breedItems.add(new ListItem(R.drawable.boxer_photo, getResources().getString(R.string.boxer_info_title), getResources().getString(R.string.boxer_info_description)));
        breedItems.add(new ListItem(R.drawable.yorkshire_terrier_photo, getResources().getString(R.string.yorkshire_terrier_info_title), getResources().getString(R.string.yorkshire_terrier_info_description)));
        breedItems.add(new ListItem(R.drawable.bulldog_photo, getResources().getString(R.string.bulldog_info_title), getResources().getString(R.string.bulldog_info_description)));
        breedItems.add(new ListItem(R.drawable.beagle_photo, getResources().getString(R.string.beagle_info_title), getResources().getString(R.string.beagle_info_description)));
        breedItems.add(new ListItem(R.drawable.pug_photo, getResources().getString(R.string.pug_info_title), getResources().getString(R.string.pug_info_description)));
        breedItems.add(new ListItem(R.drawable.chihuahua_photo, getResources().getString(R.string.chihuahua_info_title), getResources().getString(R.string.chihuahua_info_description)));
        breedItems.add(new ListItem(R.drawable.poodle_photo, getResources().getString(R.string.poodle_info_title), getResources().getString(R.string.poodle_info_description)));
        breedItems.add(new ListItem(R.drawable.rottweiler_photo, getResources().getString(R.string.rottweiler_info_title), getResources().getString(R.string.rottweiler_info_description)));

        return breedItems;
    }

    public ArrayList<ListItem> getCats() {
        ArrayList<ListItem> breedItems = new ArrayList<>();
        breedItems.add(new ListItem(R.drawable.russian_photo, getResources().getString(R.string.russian_info_title), getResources().getString(R.string.russian_info_description)));
        breedItems.add(new ListItem(R.drawable.british_shorthair_photo, getResources().getString(R.string.british_shorthair_info_title), getResources().getString(R.string.british_shorthair_info_description)));
        breedItems.add(new ListItem(R.drawable.persan_photo, getResources().getString(R.string.persan_info_title), getResources().getString(R.string.persan_info_description)));
        breedItems.add(new ListItem(R.drawable.cymric_photo, getResources().getString(R.string.cymric_info_title), getResources().getString(R.string.cymric_info_description)));
        breedItems.add(new ListItem(R.drawable.american_bobtail_photo, getResources().getString(R.string.american_bobtail_info_title), getResources().getString(R.string.american_bobtail_info_description)));

        return breedItems;
    }

    public ArrayList<ListItem> getBirds() {
        ArrayList<ListItem> breedItems = new ArrayList<>();
        breedItems.add(new ListItem(R.drawable.canar_photo, getResources().getString(R.string.canar_info_title), getResources().getString(R.string.canar_info_description)));
        breedItems.add(new ListItem(R.drawable.perus_photo, getResources().getString(R.string.perus_info_title), getResources().getString(R.string.perus_info_description)));

        return breedItems;
    }

    public ArrayList<ListItem> getFish() {
        ArrayList<ListItem> breedItems = new ArrayList<>();
        breedItems.add(new ListItem(R.drawable.betta_photo, getResources().getString(R.string.betta_info_title), getResources().getString(R.string.betta_info_description)));
        breedItems.add(new ListItem(R.drawable.discus_photo, getResources().getString(R.string.discus_info_title), getResources().getString(R.string.discus_info_description)));

        return breedItems;
    }

    public ArrayList<ListItem> getReptiles() {
        ArrayList<ListItem> breedItems = new ArrayList<>();
        breedItems.add(new ListItem(R.drawable.water_dragon_photo, getResources().getString(R.string.water_dragon_info_title), getResources().getString(R.string.water_dragon_info_description)));
        breedItems.add(new ListItem(R.drawable.testoasa_photo, getResources().getString(R.string.testoasa_info_title), getResources().getString(R.string.testoasa_info_description)));

        return breedItems;
    }

    public ArrayList<ListItem> getRodents() {
        ArrayList<ListItem> breedItems = new ArrayList<>();
        breedItems.add(new ListItem(R.drawable.rabbit_photo, getResources().getString(R.string.rabbit_info_title), getResources().getString(R.string.rabbit_info_description)));
        breedItems.add(new ListItem(R.drawable.hamster_chinezesc_photo, getResources().getString(R.string.hamster_info_title), getResources().getString(R.string.hamster_info_description)));
        breedItems.add(new ListItem(R.drawable.chinchilla_photo, getResources().getString(R.string.chinchilla_info_title), getResources().getString(R.string.chinchilla_info_description)));
        breedItems.add(new ListItem(R.drawable.guinea_pig_photo, getResources().getString(R.string.guinea_info_title), getResources().getString(R.string.guinea_info_description)));

        return breedItems;
    }

}
