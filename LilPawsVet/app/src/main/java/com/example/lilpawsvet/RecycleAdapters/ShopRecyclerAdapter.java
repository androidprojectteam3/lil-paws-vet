package com.example.lilpawsvet.RecycleAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lilpawsvet.R;
import com.example.lilpawsvet.Items.ListItem;

import java.util.ArrayList;

public class ShopRecyclerAdapter extends RecyclerView.Adapter<ShopRecyclerAdapter.ShopItemsHolder> {

    private ArrayList<ListItem> listItems;

    public ShopRecyclerAdapter(ArrayList<ListItem> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ShopItemsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_item, parent, false);
        ShopItemsHolder shopItemsHolder = new ShopItemsHolder(view);
        return shopItemsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopItemsHolder holder, int position) {
        holder.image.setImageResource(listItems.get(position).getImage());
        holder.title.setText(listItems.get(position).getTitle());
        holder.description.setText(listItems.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public static class ShopItemsHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title;
        TextView description;

        public ShopItemsHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.images);
            title = itemView.findViewById(R.id.titles);
            description = itemView.findViewById(R.id.descriptions);
        }
    }
}
