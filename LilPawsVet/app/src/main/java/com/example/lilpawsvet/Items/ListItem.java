package com.example.lilpawsvet.Items;

public class ListItem {
    private int image;
    private String title;
    private String description;

    public ListItem(int imageResource, String title, String description) {
        this.image = imageResource;
        this.title = title;
        this.description = description;
    }

    public int getImage() {
        return this.image;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }
}
