package com.example.lilpawsvet.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.os.Bundle;

import com.example.lilpawsvet.Database.Database;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.ui.Fragments.StartAnimationFragment;

public class LoginActivity extends AppCompatActivity {

    public static Database appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        appDatabase = Room.databaseBuilder(getApplicationContext(), Database.class, "database").allowMainThreadQueries().build();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container_login_activity, new StartAnimationFragment());
        fragmentTransaction.commit();
    }


}