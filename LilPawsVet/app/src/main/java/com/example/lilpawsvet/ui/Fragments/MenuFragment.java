package com.example.lilpawsvet.ui.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.lilpawsvet.Activites.AppointmentActivity;
import com.example.lilpawsvet.Activites.InfoActivity;
import com.example.lilpawsvet.R;
import com.example.lilpawsvet.Activites.ShopActivity;

import java.util.List;

public class MenuFragment extends Fragment {

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.meniu_fragment, container, false);

        //Shop Button
        final Button buttonToShop = view.findViewById(R.id.spre_shop);
        buttonToShop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    float x = (float) 1.25;
                    float y = (float) 1.25;

                    buttonToShop.setScaleX(x);
                    buttonToShop.setScaleY(y);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    float x = 1;
                    float y = 1;

                    buttonToShop.setScaleX(x);
                    buttonToShop.setScaleY(y);

                }
                return false;
            }

        });

        buttonToShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().startActivity(new Intent(requireActivity(), ShopActivity.class));
            }
        });


        //Info Button
        final Button buttonToInfo = view.findViewById(R.id.spre_info);
        buttonToInfo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    float x = (float) 1.25;
                    float y = (float) 1.25;

                    buttonToInfo.setScaleX(x);
                    buttonToInfo.setScaleY(y);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    float x = 1;
                    float y = 1;

                    buttonToInfo.setScaleX(x);
                    buttonToInfo.setScaleY(y);
                }
                return false;
            }

        });

        buttonToInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().startActivity(new Intent(requireActivity(), InfoActivity.class));
            }
        });


        //Appointment Button
        final Button buttonToAppointment = view.findViewById(R.id.spre_appointment);
        buttonToAppointment.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    float x = (float) 1.25;
                    float y = (float) 1.25;

                    buttonToAppointment.setScaleX(x);
                    buttonToAppointment.setScaleY(y);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    float x = 1;
                    float y = 1;

                    buttonToAppointment.setScaleX(x);
                    buttonToAppointment.setScaleY(y);

                }
                return false;
            }

        });

        buttonToAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().startActivity(new Intent(requireActivity(), AppointmentActivity.class));
            }
        });


        //Veterinar Button
        final Button buttonToVetInfo = view.findViewById(R.id.spre_info_vet);
        buttonToVetInfo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    float x = (float) 1.25;
                    float y = (float) 1.25;

                    buttonToVetInfo.setScaleX(x);
                    buttonToVetInfo.setScaleY(y);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    float x = 1;
                    float y = 1;

                    buttonToVetInfo.setScaleX(x);
                    buttonToVetInfo.setScaleY(y);

                }
                return false;
            }

        });
        buttonToVetInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_menu_activity, new VetFragment()).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}